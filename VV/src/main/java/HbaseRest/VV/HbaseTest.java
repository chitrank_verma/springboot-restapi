package HbaseRest.VV;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.HBaseAdmin;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.security.UserGroupInformation;
import org.springframework.stereotype.Repository;




public class HbaseTest {


	public static void main(String[] args) throws IOException {
		HbaseTest test = new HbaseTest();
		System.out.println("Calling ");
		test.createData();
	}
	public void createData() throws IOException {
	
		List<String> list1 = new ArrayList<String>();
		Configuration configuration = HBaseConfiguration.create();
		//String POSTS_TABLE = "emp";
		//String MESSAGE_COLUMN_FAMILTY="personal";

//		configuration.set("hbase.zookeeper.quorum","vvhwdevnode1.aws.virginvoyages.com,vvhwdevnode2.aws.virginvoyages.com,vvhwdevnode3.aws.virginvoyages.com");
//		configuration.set("hbase.zookeeper.property.clientPort", "2181");
//		configuration.set("zookeeper.znode.parent","/hbase-secure");
//		configuration.set("hbase.master.kerberos.principal", "hbase/vvhwdevnode1.aws.virginvoyages.com@VVHWAD.AWS.VIRGINVOYAGES.COM");
//		configuration.set("hbase.regionserver.kerberos.principal", "hbase/vvhwdevnode1.aws.virginvoyages.com@VVHWAD.AWS.VIRGINVOYAGES.COM");
//
//		//Configuration conf = new Configuration();
//		configuration.set("hadoop.security.authentication", "Kerberos");
//		UserGroupInformation.setConfiguration(configuration);
		//UserGroupInformation.loginUserFromKeytab("hbase/vvhwdevnode1.aws.virginvoyages.com@VVHWAD.AWS.VIRGINVOYAGES.COM", "D://Users//chverma//Desktop//hbase.service.keytab");
		//UserGroupInformation.loginUserFromKeytab("chverma@VVHWAD.AWS.VIRGINVOYAGES.COM", "D://keytabs//krb5.keytab");
				
		Connection connection = ConnectionFactory.createConnection(configuration);
		HTable hTable = new HTable(configuration, "sailor");

	      // Instantiating Put class
	      // accepts a row name.
	      Put p = new Put(Bytes.toBytes("4")); 

	      // adding values using add() method
	      // accepts column family name, qualifier/row name ,value
	      p.add(Bytes.toBytes("personal"),
	      Bytes.toBytes("name"),Bytes.toBytes("Kirill"));

	      p.add(Bytes.toBytes("personal"),
	      Bytes.toBytes("city"),Bytes.toBytes("Atlanta"));

	      p.add(Bytes.toBytes("tribe"),Bytes.toBytes("tribe_name"),
	      Bytes.toBytes("Sports"));

	      p.add(Bytes.toBytes("tribe"),Bytes.toBytes("sub_tribe"),
	      Bytes.toBytes("Athlete"));
	      
	      // Saving the put Instance to the HTable.
	      hTable.put(p);
	      System.out.println("data inserted");
	      
	      // closing HTable
	      hTable.close();

		

	}


}